import Vue from 'vue'
import AirbnbStyleDatepicker from 'vue-airbnb-style-datepicker'
import 'vue-airbnb-style-datepicker/dist/vue-airbnb-style-datepicker.min.css'

const datepickerOptions = {
	colors: {
    selected: '#9e8640',
    inRange: '#cdaf4f',
    selectedText: '#fff',
    text: '#565a5c',
    inRangeBorder: '#9e8640',
    disabled: '#fff',
    hoveredInRange: '#cdaf4f'
  }
}

Vue.use(AirbnbStyleDatepicker, datepickerOptions)