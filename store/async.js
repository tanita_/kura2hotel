import axios from 'axios'

export const doAsync = (store, { url, mutationTypes, callback, cached }) => {
  if (cached == null) {
    if (store.state.hasOwnProperty(mutationTypes.stateKey)) {
      return store.state[mutationTypes.stateKey]
    }
  } else if (typeof cached === 'function' && cached(store)) {
    return store.state[mutationTypes.stateKey]
  }

  store.commit(mutationTypes.BASE, {
    type: mutationTypes.PENDING,
    value: true
  })

  const lang = store.state.locale
  axios.defaults.headers.common['X-App-Locale'] = lang

  return axios(url)
    .then(response => {
      let data = response.data

      if (callback) {
        data = callback(response.data)
      }

      store.commit(mutationTypes.BASE, {
        type: mutationTypes.SUCCESS,
        data,
        statusCode: response.status
      })

      store.commit(mutationTypes.BASE, {
        type: mutationTypes.PENDING,
        value: false
      })
    })
    .catch(error => {
      store.commit(mutationTypes.BASE, {
        type: mutationTypes.FAILURE,
        statusCode: error.response.status
      })

      store.commit(mutationTypes.BASE, {
        type: mutationTypes.PENDING,
        value: false
      })
    })
}
