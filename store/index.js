import { types } from './types'

export const state = () => ({
	locales: ['en', 'id', 'jp', 'ch'],
  locale: 'en',
  link: ''
})

export const mutations = {
  SET_LANG(state, locale) {
    if(state.locales.indexOf(locale) !== -1) {
      let golink = ''
      switch(locale) {
        case 'en':
          golink = '/en/';
          break;
        case 'id':
          golink = '/id/'
          break;
        case 'ch':
          golink = '/ch/'
          break;
        default:
          golink = '/';
      }
      state.locale = locale
      state.link = golink
    }
  }
}