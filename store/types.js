import camelCase from 'lodash/camelCase'

export const createAsyncMutation = type => ({
  BASE: `${type}`,
  SUCCESS: `${type}_SUCCESS`,
  FAILURE: `${type}_FAILURE`,
  PENDING: `${type}_PENDING`,
  loadingKey: `${camelCase(type)}Pending`,
  statusCode: `${camelCase(type)}StatusCode`,
  stateKey: `${camelCase(type)}Data`
})